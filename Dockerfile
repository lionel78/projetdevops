# Utiliser une image de base officielle Python
FROM python:3.8-slim

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers de l'application Flask dans le conteneur
COPY . /app

# Installer Flask et toute autre dépendance requise
RUN pip install Flask

# Exposer le port sur lequel l'application Flask sera accessible
EXPOSE 5000
#recuperer l'app
ENV FLASK_APP=app.py

# Démarrer directement l'application Flask avec Python
#CMD ["python", "app.py"]

# Définir la commande pour démarrer l'application Flask
CMD ["flask", "run", "--host=0.0.0.0"]
